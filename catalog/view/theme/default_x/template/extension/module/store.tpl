<div class="storeListBlock">
  <h2 class="storeListBlock__title mainTitle"><?php echo $heading_title; ?></h2>
  <div class="list-group">
    <?php foreach ($stores as $store) { ?>
    <?php if ($store['store_id'] == $store_id) { ?>
    <a href="<?php echo $store['url']; ?>" class="list-group-item active"><?php echo $store['name']; ?></a>
    <?php } else { ?>
    <a href="<?php echo $store['url']; ?>" class="list-group-item"><?php echo $store['name']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
</div>