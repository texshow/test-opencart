<div class="htmlBlock">
  <?php if ($heading_title) { ?>
  <h2 class="htmlBlock__title mainTitle"><?php echo $heading_title; ?></h2>
  <?php } ?>
  <div class="htmlBlock__content htmlBlock__content-oc2 styled-block"><?php echo $html; ?></div>
</div>