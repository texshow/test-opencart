<div class="swiper-home">
  <div class="swiper-viewport">
    <div id="slideshowHome" class="slideshowHome swiper-container">
      <div class="swiper-wrapper">
        <?php foreach ($banners as $banner) { ?>
        <div class="slideshowHome__item slideItem swiper-slide" style="background-image: url('<?php echo $banner['image']; ?>')">
          <?php if ($banner['description']) { ?>
          <div class="container">
            <div class="slideItem__wrapper" <?php if ($banner['position']) { ?>style="justify-content:<?php if ($banner['position'] == 1) { ?>flex-end<?php } elseif ($banner['position'] == 2) { ?>center<?php } ?>"<?php } ?>>
              <div class="slideItem__inner">
                <?php if ($banner['link']) { ?>
                <h2 class="slideItem__title"><a href="<?php echo $banner['link']; ?>"><?php echo $banner['title']; ?></a></h2>
                <p class="slideItem__text"><?php echo $banner['description']; ?></p>
                <?php if ($banner['text_button']) { ?>
                <a class="slideItem__link" href="<?php echo $banner['link']; ?>"><?php echo $banner['text_button']; ?></a>
                <?php } ?>
                <?php } else { ?>
                <h2 class="slideItem__title"><?php echo $banner['title']; ?></h2>
                <p class="slideItem__text"><?php echo $banner['description']; ?></p>
                <?php } ?>
              </div>
            </div>
          </div>
          <?php } elseif ($banner['link']) { ?>
          <a class="slideItem__link slideItem__link-alone" href="<?php echo $banner['link']; ?>"></a>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="swiper-pagination slideshowHome__pagination"></div>
    <div class="swiper-pager slideshowHome__buttons">
      <div class="swiper-button-next slideshowHome__buttons-next"></div>
      <div class="swiper-button-prev slideshowHome__buttons-prev"></div>
    </div>
  </div>
  <script>
    $('#slideshowHome').swiper({
      mode: 'horizontal',
      slidesPerView: 1,
      pagination: '.slideshowHome__pagination',
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      autoplay: 4000,
      autoplayDisableOnInteraction: true,
      loop: true
    });
  </script>
</div>