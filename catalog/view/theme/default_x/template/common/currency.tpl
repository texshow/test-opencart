<?php if (count($currencies) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">
  <button class="menuDropdown__link submenu__link"><?php foreach ($currencies as $currency) { ?> <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?> <span class="icon"><strong><?php echo $currency['symbol_left']; ?></strong></span>
    <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?> <span class="icon"><strong><?php echo $currency['symbol_right']; ?></strong></span>
    <?php } ?> <?php } ?> <?php echo $text_currency; ?></button>
  <div class="menuDropdown__wrapper submenu__element">
    <span class="closeButton submenu__close visible-sm visible-xs"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
    <div class="submenu__wrapper">
      <ul class="menuDropdown list-unstyled">
        <?php foreach ($currencies as $currency) { ?> <?php if ($currency['symbol_left']) { ?>
        <li>
          <button class="menuDropdown__link currency-select" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?></button>
        </li>
        <?php } else { ?>
        <li>
          <button class="menuDropdown__link currency-select" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?></button>
        </li>
        <?php } ?> <?php } ?>
      </ul>
    </div>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
<?php } ?>