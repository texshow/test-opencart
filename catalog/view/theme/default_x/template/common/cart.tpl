<span class="listMenu__link miniCart" onclick="openSection('listMenu__cart')"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $text_cart_menu; ?></span>
<div id="cart" class="listMenu__wrapper listMenu__cart">
  <span class="closeButton visible-sm visible-xs" onclick="closeSection('listMenu__cart')"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
  <ul class="menuDropdown list-unstyled">
    <?php if ($products || $vouchers) { ?>
    <li class="menuDropdown__element menuDropdown__products">
      <table class="table">
        <?php foreach ($products as $product) { ?>
        <tr class="cartProducts">
          <td class="cartProducts__img text-left"><?php if ($product['thumb']) { ?>
            <a class="cartProducts__link" href="<?php echo $product['href']; ?>">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
            </a> <?php } ?></td>
          <td class="cartProducts__name text-left">
            <a class="cartProducts__link" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a> <?php if ($product['option']) { ?> <?php foreach ($product['option'] as $option) { ?>
            <br /> -
            <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small> <?php } ?> <?php } ?> <?php if ($product['recurring']) { ?>
            <br /> -
            <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small> <?php } ?></td>
          <td class="cartProducts__quantity text-right">x <?php echo $product['quantity']; ?></td>
          <td class="cartProducts__total text-right"><?php echo $product['total']; ?></td>
          <td class="cartProducts__remove text-center">
            <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="cartProducts__removeButton">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr class="cartVouchers">
          <td class="cartVouchers__name text-left" colspan="2"><?php echo $voucher['description']; ?></td>
          <td class="cartVouchers__quantity text-right">x&nbsp;1</td>
          <td class="cartVouchers__total text-right"><?php echo $voucher['amount']; ?></td>
          <td class="cartVouchers__remove text-center">
            <button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="cartVouchers__removeButton cartProducts__removeButton">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <li class="menuDropdown__element">
      <div>
        <table class="table">
          <?php foreach ($totals as $total) { ?>
          <tr class="cartInfo">
            <td class="cartInfo__description text-left">
              <?php echo $total['title']; ?>: <?php echo $total['text']; ?>
            </td>
          </tr>
          <?php } ?>
        </table>
        <p class="cartButtons text-left">
          <a class="cartButtons__link" href="<?php echo $cart; ?>">
            <i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?>
          </a>&nbsp;&nbsp;&nbsp;
          <a class="cartButtons__link" href="<?php echo $checkout; ?>">
            <i class="fa fa-share"></i> <?php echo $text_checkout; ?>
          </a>
        </p>
      </div>
    </li>
    <?php } else { ?>
    <li class="menuDropdown__element">
      <p class="menuDropdown__link"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>