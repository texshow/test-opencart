<?php if (count($languages) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
  <button class="menuDropdown__link submenu__link"><i class="fa fa-globe" aria-hidden="true"></i> <?php echo $text_language; ?>
  </button>
  <div class="menuDropdown__wrapper submenu__element">
    <span class="closeButton submenu__close visible-sm visible-xs"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
    <div class="submenu__wrapper">
      <ul class="menuDropdown list-unstyled">
        <?php foreach ($languages as $language) { ?>
        <li>
          <button class="menuDropdown__link language-select" type="button" name="<?php echo $language['code']; ?>"><img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png"
              alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></button>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
<?php } ?>