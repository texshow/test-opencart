</main>
<footer class="footer">
  <div class="container">
    <div class="row footerLinks">
      <?php if ($informations) { ?>
      <div class="col-sm-3 footerLinks__block">
        <h5 class="footerLinks__title"><?php echo $text_information; ?></h5>
        <ul class="footerLinks__list list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3 footerLinks__block">
        <h5 class="footerLinks__title"><?php echo $text_service; ?></h5>
        <ul class="footerLinks__list list-unstyled">
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3 footerLinks__block">
        <h5 class="footerLinks__title"><?php echo $text_extra; ?></h5>
        <ul class="footerLinks__list list-unstyled">
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3 footerLinks__block">
        <h5 class="footerLinks__title"><?php echo $text_account; ?></h5>
        <ul class="footerLinks__list list-unstyled">
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li class="footerLinks__listElement"><a class="footerLinks__link" href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <div class="row footerPowered">
      <div class="col-sm-12 footerPowered__block">
        <ul class="footerPowered__list footerPowered__powered list-unstyled pull-left">
          <li class="footerPowered__listElement"><p class="footerPowered__text"><?php echo $powered; ?></p></li>
        </ul>
        <ul class="footerPowered__list footerPowered__links list-inline pull-left">
          <?php if ($instagram) { ?>
          <li class="footerPowered__listElement"><a class="footerPowered__link" href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
          <?php } ?>
          <?php if ($vk) { ?>
          <li class="footerPowered__listElement"><a class="footerPowered__link" href="<?php echo $vk; ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
          <?php } ?>
          <?php if ($twitter) { ?>
          <li class="footerPowered__listElement"><a class="footerPowered__link" href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <?php } ?>
          <?php if ($facebook) { ?>
          <li class="footerPowered__listElement"><a class="footerPowered__link" href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</footer>
<script src="catalog/view/theme/default_x/javascript/libs/jquery.equalheight.min.js"></script>
<script src="catalog/view/theme/default_x/javascript/all.js"></script>
</body>

</html>