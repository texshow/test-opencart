<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="catalog/view/theme/default_x/javascript/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="catalog/view/theme/default_x/stylesheet/stylesheet.min.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/theme/default_x/javascript/common.js"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>

<body onload="init()">

<header class="header">
  <div class="container">
    <div class="row">

      <div class="col-sm-4 col-xs-10">
        <div id="logo" class="logo">
          <?php if ($logo) { ?>
          <a class="logo__link" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo__img img-responsive"
            /></a>
          <?php } else { ?>
          <h1 class="logo__title"><a class="logo__link" href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>

      <div class="col-sm-8 col-xs-2">
        <nav class="listMenu pull-right">
        
          <span class="listMenu__link bars visible-sm visible-xs" onclick="openSlideNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>
        
          <div class="listMenu__wrapperOut">
            <span class="closeButton visible-sm visible-xs" onclick="closeSlideNav()"><i class="fa fa-times" aria-hidden="true"></i></span>
        
            <!-- Categories -->
        
            <div class="listMenu__element">
              <span class="listMenu__link" onclick="openSection('listMenu__categories')"><i class="fa fa-shopping-bag" aria-hidden="true"></i>
                <?php echo $text_category; ?></span>
              <div class="listMenu__wrapper listMenu__categories">
                <span class="closeButton visible-sm visible-xs" onclick="closeSection('listMenu__categories')"><i class="fa fa-chevron-right"
                    aria-hidden="true"></i></span>
                <?php if ($categories) { ?>
                <ul class="menuDropdown list-unstyled">
                  <?php foreach ($categories as $category) { ?> <?php if ($category['children']) { ?>
                  <li class="menuDropdown__element submenu">
                    <a class="menuDropdown__link submenu__link" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                    <div class="menuDropdown__wrapper submenu__element">
                      <span class="closeButton submenu__close visible-sm visible-xs"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                      <div class="submenu__wrapper">
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <ul class="menuDropdown list-unstyled">
                          <?php foreach ($children as $child) { ?>
                          <li class="menuDropdown__element">
                            <a class="menuDropdown__link" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                          </li>
                          <?php } ?>
                        </ul>
                        <?php } ?>
                        <ul class="menuDropdown list-unstyled see-all visible-sm visible-xs">
                          <li class="menuDropdown__element">
                            <a class="menuDropdown__link" href="<?php echo $category['href']; ?>"><?php echo $text_all; ?></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <?php } else { ?>
                  <li class="menuDropdown__element">
                    <a class="menuDropdown__link main-category" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                  </li>
                  <?php } ?> <?php } ?>
                </ul>
                <?php } else { ?>
                <ul class="menuDropdown list-unstyled">
                  <li class="menuDropdown__element">
                    <p class="menuDropdown__link"><?php echo $text_empty; ?></p>
                  </li>
                </ul>
                <?php } ?>
              </div>
            </div>
        
            <!-- Cart -->
        
            <div class="listMenu__element"><?php echo $cart; ?></div>
        
            <!-- Contacts -->
        
            <div class="listMenu__element">
              <span class="listMenu__link" onclick="openSection('listMenu__contacts')"><i class="fa fa-home" aria-hidden="true"></i> <?php echo $text_contact_menu; ?></span>
              <div class="listMenu__wrapper listMenu__contacts">
                <span class="closeButton visible-sm visible-xs" onclick="closeSection('listMenu__contacts')"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                <ul class="menuDropdown list-unstyled">
                  <li class="menuDropdown__element">
                    <a class="menuDropdown__link" href="tel:<?php echo $telephone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $telephone
                     ; ?>
                    </a>
                  </li>
                  <?php if ($callback_active == 1) { ?>
                  <li class="menuDropdown__element">
                    <a class="menuDropdown__link" id="button_feedback_header" data-toggle="modal" href="#modalFeedbackHeader"><i class="fa fa-users"
                        aria-hidden="true"></i> <?php echo $text_button_callback; ?></a>
                  </li>
                  <?php } ?>
                  <li class="menuDropdown__element">
                    <a class="menuDropdown__link" href="<?php echo $contact; ?>"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $text_contact
                     ; ?>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
        
            <!-- Menu -->
        
            <div class="listMenu__element">
              <span class="listMenu__link" onclick="openSection('listMenu__menu')"><i class="fa fa-bars" aria-hidden="true"></i> <?php echo $text_menu; ?></span>
              <div class="listMenu__wrapper listMenu__menu">
                <span class="closeButton visible-sm visible-xs" onclick="closeSection('listMenu__menu')"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                <ul class="menuDropdown list-unstyled">
                  <li class="menuDropdown__element submenu">
                    <span class="menuDropdown__link submenu__link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo $text_account; ?>
                    </span>
                    <div class="menuDropdown__wrapper submenu__element">
                      <span class="closeButton submenu__close visible-sm visible-xs"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                      <ul class="menuDropdown list-unstyled">
                        <?php if ($logged) { ?>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $transaction; ?>"><?php echo $text_transaction
                           ; ?></a></li>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                        <?php } else { ?>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                        <li class="menuDropdown__element"><a class="menuDropdown__link" href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                        <?php } ?>
                      </ul>
                    </div>
                  </li>
                  <li class="menuDropdown__element"><a id="wishlist-total" class="menuDropdown__link" href="<?php echo $wishlist; ?>"><i class="fa fa-heart-o"
                        aria-hidden="true"></i> <span><?php echo $text_wishlist; ?></span></a></li>
                  <li class="menuDropdown__element submenu"><?php echo $language; ?></li>
                  <li class="menuDropdown__element submenu"><?php echo $currency; ?></li>
                  <li class="menuDropdown__element"><?php echo $search; ?></li>
                </ul>
              </div>
            </div>
        
          </div>
        </nav>
        </div>

    </div>
  </div>
</header>

<!-- Modal window callback -->

<?php if ($callback_active == 1) { ?>
<div class="modal fade" id="modalFeedbackHeader" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div style="display:inline-block; width: 100%; text-align:right;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form class="form-horizontal" id="form-feedback-header">
          <div class="form-group required">
            <div class="col-sm-12">
              <label class="control-label" for="input-name-header"><?php echo $entry_name; ?></label>
              <input type="text" name="name" value="<?php echo $name_callback; ?>" id="input-name-header" class="form-control" />
            </div>
            <div class="col-sm-12">
              <label class="control-label" for="input-phone-header"><?php echo $entry_phone; ?></label>
              <input type="text" name="phone" value="<?php echo $phone_callback; ?>" id="input-phone-header" class="form-control" />
            </div>
          </div>
        </form>
        <button type="button" id="button_send_feedback_header" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $text_send; ?></button>
      </div>
    </div>
  </div>
</div>
<script>
  $('#button_send_feedback_header').on('click', function () {
    $.ajax({
      url: 'index.php?route=common/header/write',
      type: 'post',
      dataType: 'json',
      data: $("#form-feedback-header").serialize(),
      beforeSend: function () {
        $('#button_send_feedback_header').button('loading');
      },
      complete: function () {
        $('#button_send_feedback_header').button('reset');
      },
      success: function (json) {
        $('.alert-success, .alert-danger').remove();
        if (json['error']) {
          $('#content').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        if (json['success']) {
          $('#content').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          $('input[name=\'name\']').val('');
          $('input[name=\'phone\']').val('');
        }
      }
    });
  });
</script>
<?php } ?>

<main class="main">